import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {EmployeeComponent} from './employee/employee.component';
import {CreateEmployeeComponent} from './create-employee/create-employee.component';

const routes: Routes = [
  {path: 'home', component: EmployeeComponent},
  {path: 'create', component: CreateEmployeeComponent},
  {path: '', redirectTo: '/home', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
