import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {EmployeeService} from '../employee.service';

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.css']
})
export class CreateEmployeeComponent implements OnInit {

  // ***** Member Variables *****
  newEmployee: FormGroup;
  employee: any[];

  // ***** Constructors *****
  constructor(public employeeService: EmployeeService) { }

  ngOnInit() {

    this.newEmployee = new FormGroup({
      firstName: new FormControl(),
      lastName: new FormControl(),
      salary: new FormControl()
    });
  }

  // ***** Functions *****
  createStudent() {
    const obj = {
      firstName: this.newEmployee.controls.firstName.value,
      lastName: this.newEmployee.controls.lastName.value,
      salary: this.newEmployee.controls.salary.value
    };
    this.employeeService.create(obj).subscribe(data => {this.employee = data as any[]; });
    this.newEmployee.reset();
  }
}
