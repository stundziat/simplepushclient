import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private http: HttpClient) { }

  // ***** Functions *****

  getAllEmployees() {
    return this.http.get('//this-rocks-rested-koala.apps.pcf.pcfhost.com/employee');
  }

  create(item: object) {
    console.log(item);
    return this.http.post('//this-rocks-rested-koala.apps.pcf.pcfhost.com/create', item);
  }

}
