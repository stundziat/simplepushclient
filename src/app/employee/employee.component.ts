import { Component, OnInit } from '@angular/core';
import { EmployeeService} from '../employee.service';
import {Employee} from '../Employee';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  // ***** Member Variables *****
  employeeDatabase: any;
  employees: Array<Employee> = new Array<Employee>();

  // ***** Constructors *****
  constructor(public employeeService: EmployeeService) {
  }

  ngOnInit() {
    this.employeeService.getAllEmployees().subscribe(employee => {
      this.employeeDatabase = employee;
    }, () => {
    }, () => {
      this.initializeEmployees();
    });
  }

  initializeEmployees(): void {
    for (let i = 0; i < this.employeeDatabase.length; i++ ) {
      console.log(i);
      this.employees.push(new Employee(this.employeeDatabase[i].id,
        this.employeeDatabase[i].firstName,
        this.employeeDatabase[i].lastName,
        this.employeeDatabase[i].salary)); }
  }

}
